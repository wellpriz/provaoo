
import java.util.*;

public class ControleDisciplina {
    
    private ArrayList<Disciplina> listaDisciplinas;
    
    public ControleDisciplina() {
        
        listaDisciplinas = new ArrayList<Disciplina>();
    }
    
    public void adicionarDisciplinas(Disciplina umaDisciplina) {
        listaDisciplinas.add(umaDisciplina);
    }
    
    public void removerDisciplinas(Disciplina umaDisciplina) {
        listaDisciplinas.remove(umaDisciplina);
    }
    
    public void matricularAluno(Aluno umAluno, Disciplina umaDisciplina) {
        umaDisciplina.adicionar(umAluno);
        System.out.println("Aluno :" + umAluno.getNome() + " Foi Matriculado Com Sucesso");
    }
    
    public void removerMatricula(Aluno umAluno, Disciplina umaDisciplina) {
        umaDisciplina.remover(umAluno);
        System.out.println("Aluno :" + umAluno.getNome() + " Foi Desmatriculado Com Sucesso");
    }
    
    public Disciplina pesquisarNomeDisciplina(String umNome) {
        
        for (Disciplina umaDisciplina : listaDisciplinas) {
            if (umaDisciplina.getDisciplina().equalsIgnoreCase(umNome)) {
                return umaDisciplina;
                
            }
            
        }
        return null;
    }
    
    public void MostraDisciplinas() {
        for (Disciplina umaDisciplina : listaDisciplinas) {
            System.out.println("______________________________________________________________");
            System.out.println("Nome Disciplina:" + umaDisciplina.getDisciplina() + "|" + "Codigo:" + umaDisciplina.getCodigo());
            System.out.println("______________________________________________________________");
        }
        
        
    }
    
    public void MostrarAlunosMatriculados(Disciplina umaDisciplina){
       
            System.out.println("______________________________________________________________");
            System.out.println("Nome Disciplina:" + umaDisciplina.getDisciplina() + "|" + "Codigo:" + umaDisciplina.getCodigo());
            System.out.println("______________________________________________________________");
        umaDisciplina.MostraALunos();
    
    }
    
    
}
