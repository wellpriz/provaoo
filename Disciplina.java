
import java.util.*;

public class Disciplina {

    private String disciplina;
    private String codigo;
    private ArrayList<Aluno> listaAlunos;

    public Disciplina(String umNome, String umCodigo) {
        disciplina = umNome;
        codigo = umCodigo;
        listaAlunos = new ArrayList<Aluno>();
    }

    public void setDisciplina(String umaDisciplina) {
        disciplina = umaDisciplina;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setCodigo(String umCodigo) {
        codigo = umCodigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void adicionar(Aluno umAluno) {
        listaAlunos.add(umAluno);
    }

    public void remover(Aluno umAluno) {
        listaAlunos.remove(umAluno);
    }

    public void MostraALunos() {
        for (Aluno umAluno : listaAlunos) {
            System.out.println("______________________________________________________________");
            System.out.println("Nome do Aluno:" + umAluno.getNome() + "|" + "Matricula:" + umAluno.getMatricula());
            System.out.println("______________________________________________________________");
        }
    }
}
