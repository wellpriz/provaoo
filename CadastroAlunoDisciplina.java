
import java.io.*;

public class CadastroAlunoDisciplina {

    public static void main(String[] args) throws IOException {

        InputStream entradaSistema = System.in;
        InputStreamReader leitor = new InputStreamReader(entradaSistema);
        BufferedReader leitorEntrada = new BufferedReader(leitor);
        String entradaTeclado;
        char opcao;

        ControleAluno umControleAluno = new ControleAluno();
        ControleDisciplina umControleDisciplina = new ControleDisciplina();
        Aluno umAluno;


        do {
            System.out.println("======================================");
            System.out.println(" Opção 1 :\"Cadastra Aluno \"");
            System.out.println(" Opção 2 :\"Criar Disciplina \"");
            System.out.println(" Opção 3 :\"Matricula Aluno \"");
            System.out.println(" Opção 4 :\"Buscar Alunos Matriculados Ou Disciplina \"");
            System.out.println(" Opção 5 :\"Desmatricular um Aluno\"");
            System.out.println(" Opção 6 :\"Mostra Lista de Alunos Ou Disciplina\"");
            System.out.println(" Opção 7 :\"Mostra Alunos Matriculados Na disciplina\"");
            System.out.println(" Opção 8 :\"Desligar Aluno\"");
            System.out.println(" Opção 9 :\"Sair\"");
          
            

            System.out.println("======================================");
            System.out.println("Digite a opção Desejada : ");
            entradaTeclado = leitorEntrada.readLine();
            opcao = entradaTeclado.charAt(0);

            switch (opcao) {

                case '1':
                    System.out.println("Digite o nome da Aluno: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umNome = entradaTeclado;

                    System.out.println("Digite a Matricula: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umaMatricula = entradaTeclado;

                    umAluno = new Aluno(umNome, umaMatricula);
                    umControleAluno.criaAluno(umAluno);
                    break;
                case '2':
                    System.out.println("Digite o nome da Disciplina: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umNomeDisciplina = entradaTeclado;

                    System.out.println("Digite o codigo: ");
                    entradaTeclado = leitorEntrada.readLine();
                    String umCodigo = entradaTeclado;

                    Disciplina umaDisciplina = new Disciplina(umNomeDisciplina, umCodigo);
                    umControleDisciplina.adicionarDisciplinas(umaDisciplina);
                    break;
                case '3':
                    System.out.println("Digite o nome da Aluno a ser matriculado: ");
                    entradaTeclado = leitorEntrada.readLine();
                    umNome = entradaTeclado;
                    umAluno = umControleAluno.pesquisarNomeAluno(umNome);

                    System.out.println("Digite a disciplina da Aluno a ser matriculado: ");
                    entradaTeclado = leitorEntrada.readLine();
                    umNomeDisciplina = entradaTeclado;

                    umaDisciplina = umControleDisciplina.pesquisarNomeDisciplina(umNome);
                    umControleDisciplina.matricularAluno(umAluno, umaDisciplina);
                    break;
                case '4':
                    System.out.println("Digite \'D\' para Buscar Disciplina ou \'A\' para Aluno : ");
                    entradaTeclado = leitorEntrada.readLine();
                    umNome = entradaTeclado;
                    if (umNome.equalsIgnoreCase("a")) {
                        System.out.println("Digite o nome da Aluno a ser Buscado: ");
                        entradaTeclado = leitorEntrada.readLine();
                        umNome = entradaTeclado;
                        umControleAluno.pesquisarNomeAluno(umNome);
                      
                    } else if (umNome.equalsIgnoreCase("d")) {

                        System.out.println("Digite o nome da Disciplina a ser Buscada: ");
                        entradaTeclado = leitorEntrada.readLine();
                        umNome = entradaTeclado;
                        umControleDisciplina.pesquisarNomeDisciplina(umNome);
                     
                    }


                    break;
                case '5':
                    System.out.println("Digite o nome da Aluno a ser Desmatriculado: ");
                    entradaTeclado = leitorEntrada.readLine();
                    umNome = entradaTeclado;
                    umAluno = umControleAluno.pesquisarNomeAluno(umNome);

                    System.out.println("Digite a disciplina da Aluno a ser Desmatriculado: ");
                    entradaTeclado = leitorEntrada.readLine();
                    umNomeDisciplina = entradaTeclado;

                    umaDisciplina = umControleDisciplina.pesquisarNomeDisciplina(umNome);
                    umControleDisciplina.removerMatricula(umAluno, umaDisciplina);
                    break;

                case '6':
                    System.out.println("Digite \'D\' para Mostra Disciplina ou \'A\' para Aluno : ");
                    entradaTeclado = leitorEntrada.readLine();
                    umNome = entradaTeclado;
                    if (umNome.equalsIgnoreCase("a")) {

                        umControleAluno.MostraALuno();
                    } else if (umNome.equalsIgnoreCase("d")) {
                        umControleDisciplina.MostraDisciplinas();
                    }

                    break;

                case '7':
                    
                        System.out.println("Digite o nome da Disciplina a ser Buscada: ");
                        entradaTeclado = leitorEntrada.readLine();
                        umNome = entradaTeclado;
                       umaDisciplina = umControleDisciplina.pesquisarNomeDisciplina(umNome);
                       umControleDisciplina.MostrarAlunosMatriculados(umaDisciplina);
                    
                    break;

                case '8':

                      System.out.println("Digite o nome da Aluno a ser Buscado: ");
                        entradaTeclado = leitorEntrada.readLine();
                        umNome = entradaTeclado;
                        umAluno=umControleAluno.pesquisarNomeAluno(umNome);
                        umControleAluno.remover(umAluno.getNome());
                
                    
                      
                          opcao= 0;
                    break;
                     case '9':

                      System.out.println("Obrigado! Sair: ");
                          opcao= 0;
                    break;


      
                    
   



            }






        } while (opcao != 0);
    }
}
