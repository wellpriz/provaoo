
import java.util.ArrayList;

public class ControleAluno {

    private ArrayList<Aluno> listaAlunos;

    public ControleAluno() {
        listaAlunos = new ArrayList<Aluno>();
    }

    public void criaAluno(Aluno novoAluno) {
        listaAlunos.add(novoAluno);
    }

    public Aluno pesquisarNomeAluno(String umNome) {
        for (Aluno umAluno : listaAlunos) {
            if (umAluno.getNome().equalsIgnoreCase(umNome)) {
                return umAluno;
            }
        }
        System.out.println("Aluno Não encontrado");
        return null;
    }

    public void remover(String umNome) {
        for (Aluno umAluno : listaAlunos) {
            if (umAluno.getNome().equalsIgnoreCase(umNome)) {
                listaAlunos.remove(umAluno);
            }

        }
    }

    public void MostraALuno() {
        for (Aluno umAluno : listaAlunos) {
            System.out.println("______________________________________________________________");
            System.out.println("Nome do Aluno:" + umAluno.getNome() + "|" + "Matricula:" + umAluno.getMatricula());
            System.out.println("______________________________________________________________");
        }
    }
}
